% working_directory(CWD, 'C:/Users/Zakk/Desktop/UNI/5Semester/IKT212/Part_3/prologassignment').

% File is './puzzle_00.txt'.

:- use_module(library(pio)).

/*
% https://www.youtube.com/watch?v=kHGIFC3ThL0

write_to_file(File, Text):-
    open(File, write, Stream),
    write(Stream, Text), nl,
    close(Stream).

read_from_file(File):-
    open(File, read, Stream),

    get_char(Stream, Char1),

    process_the_stream(Char1, Stream, Board),
    close(Stream),

    write_to_file('test.txt', Board).

process_the_stream(end_of_file, _, _):- !.

process_the_stream(Char, Stream, Board):-
    append(Board, Char, Board),
    get_char(Stream, Char2),
    process_the_stream(Char2, Stream, Board).
*/

/*
all([]) --> [].
all([L|Ls]) --> [L], all(Ls).

reader(File):-
    open(File, read, Stream),
    stream_to_lazy_list(Stream, Board),
    close(Stream),
    open('test.txt', write, Stream2),
    write(Stream, Board),
    close(Stream2).
*/


% Fra lab!!! %

% Bruk liten x i prolog siden,
%   de vil være store X i filen du leser fra,
%   men det hadde blitt en variabel i prolog.
%   Så vi skriver dem som atoms i prolog.
puzzle1([
    [_,_,_,_],
    [_,4,_,_],
    [x,_,_,_],
    [_,_,_,_],
    [2,x,_,_],
    [_,_,_,_]
]).

puzzle2([
    [x,x,_,x],
    [x,2,_,_],
    [2,_,x,x],
    [_,x,x,x],
    [_,x,x,_],
    [x,x,x,x],
    [x,x,_,_],
    [_,_,_,_],
    [_,_,2,x]
]).

repres1([
    [_,o,_,_],
    [o,4,o,_],
    [x,o,_,_],
    [o,_,_,_],
    [2,x,_,o],
    [o,_,_,_]
]).

repres2([
    [x,x,_,x],
    [x,2,o,_],
    [2,o,x,x],
    [o,x,x,x],
    [_,x,x,o],
    [x,x,x,x],
    [x,x,_,o],
    [_,_,o,_],
    [_,o,2,x]
]).

% Commented out for the sake of testing. %
%?- puzzle1(P), solve(P,R).


solveEasy(Rep):- doSomething(Rep), doMore(Rep).

solve(P,R):- prepare(P, Repres), solveEasy(Repres), solveR(Repres, R).

% Manuelt skriv en løsning representation av puzzle1 og send den inn i stedet for puzzle1. %
prepare(X,Y):- puzzle1(X), repres1(Y).

% Commented out for the sake of testing. %
%:- consult('<name_of_other_file>').

step1([1,_,_]).

step2([_,2,_]).
step2([_,b,_]).

step3([_,_,a]).

sooolve(X):- step1(X), step2(X), step3(X).

% End fra lab! %

check_char(X):- X>=48, N=<52, number_found(X). % ASCII table values. %

number_found(0):- write('0 Found'), get_pluss_struct().
number_found(1):- write('1 Found').
number_found(2):- write('2 Found').
number_found(3):- write('3 Found').
number_found(4):- write('4 Found').

get_pluss_struct(X):- . % Retrieves the adjacent tiles around the number. %


reverse(List1, X). % To reverse a list. %
member(X,List1). % To find if an element is part of a list. %
append(List1, List2, X). % Adds List2 to the end of List1, and stores this new list in X. %

name(String1, A). % Converts a string into ascii characters and stores it in A. %
name(A, AsciiString1). % Converts the string of ascii characters back to chars, and stores them in A. %

% Writes all the elements of a list to terminal. %
write_list([]).
write_list([Head|Tail]):-
    write(Head), nl,
    write_list(Tail).

% Joins two strings together, via ascii conversion. %
join_str(S1, S2, S3):-
    name(S1, SL1),
    name(S2, SL2),
    append(SL1, SL2, SL3),
    name(S3, SL3).

% Written here, but is used from terminal as a call. %
%   Prints the letter specified with index in the nth0 call. %
name('Derek', List),
    nth0(0, List, FChar),
    put(FChar).

% Written here, but is used from terminal as a call. %
%   Gives the length of a string. %
atom_length('Derek', X).


writeLine(Line):-
    % doStuff() %
writeBoard([]).
writeBoard([H|T]):-
    writeLine(H), nl,
    writeBoard(T).


numFound(N,4):-
numFound(N,3):-
numFound(N,2):-
numFound(N,1):-
numFound(N,0):-

findNums([]).
findNums([Line|T]):- 
    nth0(N,Line,Char),
    Char >= 48, Char =< 52,
    numFound(N,Char),
    findNums(T).


string_chars(Str,Line).





/*
readBoard(B):-


readPuz(P):-


process_the_stream(nl, _):- % needs something to do %
process_the_stream(end_of_file, _):- !.
process_the_stream(Char, Stream):-
    % do something with the char %
    get_char(Stream, Char2),
    process_the_stream(Char2, Stream).

*/