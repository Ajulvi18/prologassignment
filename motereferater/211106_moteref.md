Tilstede: alle

møtestart 17:15

saker
- runden(siden sist)
    - Andreas:
        - lest på nettsider ang prolog
        - knotet litt med ide
    - Tauder:
        - begynt å se på forelesning, nesten kommet gjennom mandag
    - Mats:
        - Vært i forelesninger, skrevet notater
        - laget 4 filer, testet det han har gått gjennom
        - satt opp overleaf

- arbeidstruktur
    - scrum møter
    - neste møte 14:15
- til neste gang:
    - Andreas
        - Bli kjent med prolog
        - Puzzle løsning i prolog
        - Adventures in prolog (tilegne mer kunskap)
    - Tauder
        - se minst 2 forelesninger til
        - bli mer kjent med prolog
        - time lang tutorial
    - Mats
        - tukle mer i prolog

møte over: 17:33

