outputFile('./puzzle_solved.txt').
inputFile('./puzzle_unsolved.txt').

/********************* solving the puzzle */
doSolve(P,P).

/********************* writing the result */
writeFullOutput(puzzle(4,6)):- 
  write('size 4x6'), nl,
  write('_*__'), nl,
  write('*4*_'), nl,
  write('X*__'), nl,
  write('*___'), nl,
  write('2X_*'), nl,
  write('*___'), nl.
writeFullOutput(puzzle(4,9)):- 
  write('size 4x9'), nl,
  write('XX_X'), nl,
  write('X2*_'), nl,
  write('2*XX'), nl,
  write('*XXX'), nl,
  write('_XX*'), nl,
  write('XXXX'), nl,
  write('XX_*'), nl,
  write('__*_'), nl,
  write('_*2X'), nl.
writeFullOutput(P):- write('cannot solve puzzle: '), write(P), nl.

/********************** reading the input */
readProblem(puzzle(X,Y)):- 
  findKW(size), readInt(X), readInt(Y), readLines(X,Y).

findKW(KW):- string_codes(KW,[H|T]), peek_code(H), readKW([H|T]), !.
findKW(_):- peek_code(-1), !, fail.
findKW(KW):- get_code(_), findKW(KW).

readKW([]):- get_code(_).
readKW([H|T]):- get_code(H), readKW(T).

readHintLine(0):- get_code(_).
readHintLine(N):- N>0, N1 is N-1, get_code(_), readHintLine(N1).

readLines(_,0).
readLines(X,Y):- Y>0, Y1 is Y-1, readHintLine(X), readLines(X,Y1).

readInt(N):- get_code(M), handleCode(M,N).

handleCode(M,N):- is_number_code(M,N1), !, continueInt(N1,N).
handleCode(-1,_):- !, fail. /* EOF */
handleCode(_,N):- readInt(N).

continueInt(O,N):- get_code(M), is_number_code(M,M1), !, H is 10*O+M1, continueInt(H,N).
continueInt(N,N).

is_number_code(N, N1):- N>=48, N<58, N1 is N-48.
is_number_code(95,0).

/*********************** global control: starting the algorithm and the reading */
input_output(IF,OF):- current_prolog_flag(argv,['--io',IF,OF]),!.
input_output(IF,OF):- inputFile(IF), outputFile(OF).

run:- input_output(IF,OF), write("Reading from: "), write(IF), write(", writing to: "), write(OF), nl, see(IF), tell(OF), findKW(puzzles), readInt(N), write('puzzles '), write(N), nl, solvePuzzles(N), told, seen, !.
run:- told, seen. /* close the files */

solvePuzzles(0).
solvePuzzles(N):- N>0, readProblem(P), doSolve(P, S), writeFullOutput(S), !, N1 is N-1, solvePuzzles(N1).

:- run.
:- halt.