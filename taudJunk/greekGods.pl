parent_child(chaos, gaea).
parent_child(gaea, cyclope).
parent_child(gaea, chronos).
parent_child(gaea, coeus).
parent_child(gaea, oceanus).
parent_child(uranus, cyclope).
parent_child(uranus, chronos).
parent_child(uranus, coeus).
parent_child(uranus, oceanus).
parent_child(chronos, hades).
parent_child(chronos, poseidon).
parent_child(chronos, zeus).
parent_child(rhea, hades).
parent_child(rhea, poseidon).
parent_child(rhea, zeus).
parent_child(coeus, leto).
parent_child(phoebe, leto).
parent_child(leto, apollo).
parent_child(leto, artemis).
parent_child(zeus, apollo).
parent_child(zeus, artemis).
parent_child(oceanus, iapetus).
parent_child(tethys, iapetus).
parent_child(hera, ares).
parent_child(zeus, ares).
male(chaos).
male(cyclope).
male(uranus).
male(chronos).
male(coeus).
male(oceanus).
male(hades).
male(poseidon).
male(zeus).
male(ares).
male(apollo).
male(iapetus).
female(gaea).
female(rhea).
female(leto).
female(hera).
female(phoebe).
female(tethys).
female(artemis).

sibling(X,Y):- parent_child(Z,X), parent_child(Z,Y).
mother(X,Y):- parent_child(X,Y), female(X).
father(X,Y):- parent_child(X,Y), male(X).
grandmother(X,Y):- mother(X,Z), parent_child(Z,Y), female(X).
grandfather(X,Y):- father(X,Z), parent_child(Z,Y), male(X).
uncle(X,Y):- sibling(X,Z), parent_child(Z,Y), male(X).
aunt(X,Y):- sibling(X,Z), parent_child(Z,Y), female(X).
son(X,Y):- parent_child(Y,X), male(X).
daugther(X,Y):- parent_child(Y,X), female(X).
newphew(X,Y):- uncle(Y,X); aunt(Y,X), male(X).
niece(X,Y):- uncle(Y,X); aunt(Y,X), female(X).
