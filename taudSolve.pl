doTaudSolve(P,P):- findNums(P).

bulb("*").

findNums(puzzle(_,_,[])).
findNums(puzzle(_,_,[Line|T])):- 
    nth0(N,Line,Char),
    numFound(Line,N,Char),
    findNums(puzzle(_,_,T)).

numFound(Line,N,4):- nth0(I,Line,V), var(V),
    I=N-1; I=N+1, bulb(V), write(Line).





%numFound(Line,N,3). % do stuff for tile: 3 %
%numFound(Line,N,2). % do stuff for tile: 2 %
%numFound(Line,N,1). % do stuff for tile: 1 %
%numFound(Line,N,0). % do stuff for tile: 0 %
